#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

int main() {
    char *s = NULL;
    size_t n = 0, l;

    l = getline(&s, &n, stdin);
    for (int i = 0; i < l; i++)
        s[i] = toupper(s[i]);
    
    printf("%s", s);
    free(s); 

    return 0;
}

/*
    Valgrind tell us that the program uppercase has a memory leak tha loses 120 bytes for me 
    the leak happens from the getline function. By looking up getline function we can see that there is a hidden code of malloc() that need to be freed up with free()
*/