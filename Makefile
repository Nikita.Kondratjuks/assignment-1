
clean: 
	rm -f prime process uppercase prime fib

process.c: process.c 
	gcc process.c -o process

swap.c: swap.c 
	gcc swap.c -o swap

uppercase.c: uppercase.c 
	gcc uppercase.c -o uppercase

prime.c: prime.c 
	gcc prime.c -o prime

fib.c: fib.c
	gcc fib.c -o fib

