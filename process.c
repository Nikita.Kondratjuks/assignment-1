#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

int main() 
{
    int stat;
    int rc;
    pid_t processor[5];
    int cn;
    for (int i = 0; i < 5; i++)
    {
        rc = fork();
        if (rc == 0) {
            printf("I am a child. My status is %d\n", i, getpid());
            cn = i;
            exit(i);
        } else {
            processor[i] = rc;
        }
            
        }
        for (int i = 0; i < 5; i++)
        {
                stat = 0;
                int cpid = waitpid(processor[i], &stat, 0);

                if (WIFEXITED(stat)) 
                {
                    printf("Child %d terminated with status: %d\n", cpid, WEXITSTATUS(stat));
                }
        }
        
    return 0;        
}
