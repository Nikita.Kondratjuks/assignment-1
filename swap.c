#include <stdio.h>

void swap(int *a, int *b) {
    int t = *a;
    *a = *b;
    *b = t;
}

int main() {
    int *a = NULL, *b = NULL, x, y;

    if (scanf("%d %d", &x, &y) < 2)
        return 1;

    a = &x, b = &y; //Changed a --> b
    swap(a, b);

    printf("%d %d\n", *a, *b);

    return 0;
}
/*
    After running Valgrind on swap it says that "Invalid read of size 4" occurred on the orrigional code
    because of that b pointer is declared but never given anything to point to, we see this with both a pointing on line 15
*/
